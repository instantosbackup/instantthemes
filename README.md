<div align="center">
    <h1>instantTHEMES</h1>
    <p>Theming for instantOS</p>
    <img width="300" height="300" src="https://media.githubusercontent.com/media/instantOS/instantLOGO/master/png/theme.png">
</div>

instantTHEMES is a utility to provide theming for isntantOS
it includes

-   cursor theme (gtk and xorg)
-   gtk theme
-   qt theme
-   xresources
-   dunst theme
-   fonts
-   icon theme
-   a light and dark version

it is used by instantOS in the background and is probably of little use outside of that
It depends on paperbash to work. 

--------
### instantOS is still in early beta, contributions always welcome
