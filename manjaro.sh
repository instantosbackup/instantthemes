#!/bin/bash

########################
## manjaro like style ##
########################

themefetch() {
    echo "fetching manjaro theme"

    mkdir /tmp/manjarotheme
    cd /tmp/manjarotheme

    if ! themeexists matcha &>/dev/null; then
        git clone --depth=1 https://github.com/vinceliuice/matcha.git
        cd matcha
        ./Install
        cd ..
        rm -rf matcha
    fi

    if ! icons_exist "Papirus-Maia" &>/dev/null; then
        git clone --depth=1 https://github.com/Ste74/papirus-maia-icon-theme.git
        cd papirus-maia-icon-theme
        mkdir ~/.icons &>/dev/null
        mv Papirus* ~/.icons
        cd ..
        rm -rf papirus-maia-icon-theme
    fi
    curl -s "https://raw.githubusercontent.com/paperbenni/dotfiles/master/fonts/sourcecodepro.sh" | bash
    curl -s "https://raw.githubusercontent.com/paperbenni/dotfiles/master/fonts/roboto.sh" | bash

    if ! [ -e ~/.icons/Breeze ]; then
        mkdir ~/.icons &>/dev/null
        cd ~/.icons
        svn export "https://github.com/KDE/breeze.git/trunk/cursors/Breeze/Breeze"
    fi

}

lighttheme() {
    gtktheme "Matcha-sea"
    gtkicons "Papirus-Maia"

}

darktheme() {
    gtktheme "Matcha-dark-sea"
    gtkicons "Papirus-Dark-Maia"
}

themeapply() {

    lighttheme
    rofitheme manjaro
    dunsttheme manjaro
    xtheme manjaro
    setcursor Breeze
}
